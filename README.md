# The Trivia Game
A SPA react js Translation. 
## Table of Contents
- [Description](#Description)
- [Installation](#Installation)
- [Components](#Components)
- [Maintainer](#Maintainer)
- [Author](#Author)
- [License](#License)
## Description
This is a single page application useing react to make a english sign language translation app.
To start add a username and press "Start", then  enter a sentence to be translated and press "Translate".
Press teh profile button at the top right corner, to see info about the user.
The data for the users is provided to with RESTful API.
## Installation
Install node.js.
Clone the repo. Open with Visual studio code and the type "npm install" in the terminal.
Then type "npm start" in the terminal

## Components
This project contains componets, api, views, hoc, utils and context.
Also using store.ts and router.ts
## App Link
https://agile-escarpment-36680.herokuapp.com/
## Maintainer
[@emiliozimberlin](https://gitlab.com/emiliozimberlin)
## Author
Emilio Zimberlin
## License
[MIT](https://choosealicense.com/licenses/mit/)

