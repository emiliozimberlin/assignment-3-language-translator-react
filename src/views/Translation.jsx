import withAuth from "../hoc/withAuth";
import TranslationForm from "../components/Translation/TranslationForm";
import TranslationResult from "../components/Translation/TranslationResult";
import { useUser } from "../context/UserContext";
import { useState } from "react";
import { translationAdd } from "../api/translation";
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKey";

const Translation = () => {
  const { user, setUser } = useUser();
  const [translation, setTranslation] = useState(null);

  const displayTranslation = (text) => {
    return <TranslationResult text={text} />;
  };

  const handleTranslationClicked = async (text) => {
    const str = text.replace(/[^a-zA-Z]/g, "").toLowerCase();
    
    setTranslation(displayTranslation(str));
    const [error, result] = await translationAdd(user, text);
    if(error !== null){

    }
    storageSave(STORAGE_KEY_USER, result);
    setUser(result);
  };

  return (
    <>
      <h1>Translation</h1>

      <section id="translation-list">
        <TranslationForm onTranslation={handleTranslationClicked} />
        {translation}
      </section>
    </>
  );
};
export default withAuth(Translation);
