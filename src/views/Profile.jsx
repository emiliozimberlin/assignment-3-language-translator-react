import withAuth from "../hoc/withAuth";
import ProfileHeaders from "../components/Profile/ProfileHeaders";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileTranslations from "../components/Profile/ProfileTranslations";
import { useUser } from "../context/UserContext";
import { useEffect } from "react";
import { userFindById } from "../api/user";
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKey";

const Profile = () => {
  const { user, setUser } = useUser();
  useEffect(() => {
    const findUser = async () => {
      const [error, currentUser] = await userFindById(user.id);
      if (error === null) {
        storageSave(STORAGE_KEY_USER,currentUser)
        setUser(currentUser);
      }
    };
    findUser();
  },[setUser, user.id]);
  return (
    <>
      <h1>Profile</h1>
      <ProfileHeaders username={user.username} />
      <ProfileActions />
      <ProfileTranslations translations={user.translations} />
    </>
  );
};
export default withAuth(Profile);
