export const storageSave = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value));
  validateKey(key);
  if (!value) {
    throw new Error("storageSave: No value provided for" + key);
  }
};

export const storageRead = (key) => {
  validateKey(key);
  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};

export const storageDelete = (key) => {
  sessionStorage.removeItem(key);
};

const validateKey = (key) => {
  if (!key || typeof key !== "string") {
    throw new Error("No key provided or invalid key type");
  }
};
