import { useForm } from "react-hook-form";

const translationConfig = {
  maxLength: 40,
};

// gets the input text and translate it to hand sings
const TranslationForm = ({ onTranslation }) => {
  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm();

  const onSubmit = ({ text }) => {
    onTranslation(text);
  };

  const errorMsg = (() => {
    if (!errors.text) {
      return null;
    }

    if (errors.text.type === "maxLength") {
      return <span>Sentence is too long, max 40 letters</span>;
    }
  })();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset>
        <label htmlFor="translation-list"></label>

        <input
          type="text"
          {...register("text", translationConfig)}
          placeholder="Enter a sentence"
        />
        {errorMsg}
      </fieldset>
        <button className="btn" type="submit">Translate</button>
    </form>
  );
};
export default TranslationForm;
