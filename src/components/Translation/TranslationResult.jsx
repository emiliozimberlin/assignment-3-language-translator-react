const TranslationResult = ({ text }) => {
  const letters = text.split("");
  console.log("IM TRASN " + text);

  const signLetters = letters.map((letter, idx) => {
    const imgPath = `img/signs/${letter}.png`;

    // renders the list with images matching the letters
    return (
      <li className="liSigns"  key={idx}>
        <img src={imgPath} alt={letter} width="60"/>
      </li>
    );

  });

  return (
    <section className="box1">
      <h3>Translated text:</h3>
      <ul className="translateSigns">{signLetters}</ul>
    </section>
  );
};

export default TranslationResult;
