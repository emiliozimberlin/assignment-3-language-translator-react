import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";
const Navbar = () => {
  const { user } = useUser();
  return (
    <nav>
      {user !== null && (
        <div className="navDiv">
          <ul>
            <li className="navTranslation">
              <NavLink to="/translations">Translations</NavLink>
            </li>
            <li className="navProfile">
              <NavLink to="/profile">Profile</NavLink>
            </li>
          </ul>
        </div>
      )}
    </nav>
  );
};
export default Navbar;
