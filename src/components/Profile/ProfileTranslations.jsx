import ProfileTranslationsItems from "./ProfileTranslationsItems";

// renders the translation list
const ProfileTranslations = ({ translations }) => {
  const translationList = translations.map((item, idx) => (
    <ProfileTranslationsItems key={idx + "-" + item} item={item} />
  ));

  return (
    <section className="box1">
      <h4>Your translations</h4>
      <ul className="translateSigns">{translationList}</ul>
    </section>
  );
};
export default ProfileTranslations;
