import { Link } from "react-router-dom";
import { storageDelete, storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKey";
import { useUser } from "../../context/UserContext";
import { translationDelete } from "../../api/translation";

const ProfileActions = () => {
  const { user, setUser } = useUser();
  // logs out the current user 
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  // removes all the translations from the current user
  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure ?")) {
      return;
    }
    const [clearError] = await translationDelete(user.id);

    if(clearError !== null){
      
      return
    }
    const translationCleared = {
      ...user, 
      translations: []
    }
    storageSave(STORAGE_KEY_USER, translationCleared);
    setUser(translationCleared);
  };

  return (
    <>
      <button className="logoutBtn" onClick={handleLogoutClick}>Logout</button>
      <button className="deleteBtn" onClick={handleClearHistoryClick}>Delete</button>
    </>
  );
};
export default ProfileActions;
