import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useState, useEffect } from "react";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKey";

const usernameConfig = {
  required: true,
  minLength: 2,
};
// hooks
const LoginFrom = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();
  
  // local state
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);
  
  // Side Effects
  useEffect(() => {
    if (user !== null) {
      navigate("translations");
    }
  }, [user, navigate]);

  // adds a user to the api or gets the user if exists
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);

    if (error) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }

    setLoading(false);
  };
  // shows error massage if needed
  const errorMsg = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span>Name is required</span>;
    }
    if (errors.username.type === "minLength") {
      return <span>Name is too short! Minimun two letters</span>;
    }
  })();
  return (
    <>
      <h2>What is your name?</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <label htmlFor="username"></label>
          <input
            type="text"
            placeholder="Enter your name"
            {...register("username", usernameConfig)}
          />
          {errorMsg}
        </fieldset>
        <button className="btn" type="submit" disabled={loading}>
          Start
        </button>
        {loading && <p>Starting up...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
    </>
  );
};
export default LoginFrom;
