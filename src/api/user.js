import { apiKey, url } from "./index";


// checks if a user exists
export const userExists = async (username) => {
  try {
    const response = await fetch(`${url}?username=${username}`);
    if (!response.ok) {
      throw new Error("Can not create user with name" + username);
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};
// creates a new user and adds it to the API
export const createUser = async (username) => {

  try {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
          'X-API-Key': apiKey,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username,
            translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error("Can not complete request");
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};
// checks if the user exists if so gets the user, else creates a new user.
export const loginUser = async (username) => {

  const [hasError, user] = await userExists(username);

  if (hasError !== null) {
    return [hasError, null];
  }

  if (user.length > 0) {
    return [null, user.pop()];
  }

  return await createUser(username);
};
export const userFindById = async (userId) => {
  try {
    const response = await fetch(`${url}/${userId}`);
    if (!response.ok) {
      throw new Error("Can find user with id " + userId);
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, null];
  }
}